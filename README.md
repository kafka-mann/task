# Kafka-Mann Classification Task

Here is the main repository for the Kafka-Mann Classification Task. The
Kafka-Mann Classification Task has the following goal:

> Build the best system to classify whether a sentence is from Franz Kafka
> or Thomas Mann.

Therefore we provide a training, development and test set to build such a
system.

The dataset can be found in this [repository](https://gitlab.com/kafka-mann/dataset).

This repository gives an overview of the best systems and their results on the
final test set. If you trained a system, please open a pull request to get
your system listed here! Please refer to the [Contributing](CONTRIBUTING.md) guide.
Notice: the implementation of the system must be freely available.

# Systems

Here comes an overview of the best systems for the Kafka-Mann Classification
Task.

| System Name                              | System Repository                                       | Accuracy development set | Accuracy test set
| ---------------------------------------- | ------------------------------------------------------- | ------------------------ | -----------------
| Stacked CNNs, Pretrained Word Embeddings | [here](https://gitlab.com/stefan-it/kafka-mann-cnn-emb) | 0.8439                   | n.a.

# Contact (Bugs, Feedback, Contribution and more)

For questions about the task repository, contact the current maintainer:
Stefan Schweter <stefan@schweter.it>. If you want to contribute to the project
please refer to the [Contributing](CONTRIBUTING.md) guide!

# License

To respect the Free Software Movement and the enormous work of Dr. Richard Stallman
the software in this repository is released under the *GNU Affero General Public License*
in version 3. More information can be found [here](https://www.gnu.org/licenses/licenses.html)
and in `COPYING`.
